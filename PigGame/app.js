/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

// a single var will keep the scores - this will be inside an array
var scores, roundScore, activePlayer;

scores = [0, 0];
roundScore = 0;     // we only have a single roundScore at a time.
activePlayer = 0;   // activePlayer 0 is the first player and 1 is the second player

// the element can also just be selected without modifying the element 
// this is a getter - this just GETS a value
var x = document.querySelector("#current-0").textContent;
console.log(x);

// this hides the dice icon
document.querySelector('.dice').style.display = 'none';

// set all values to 0
document.getElementById('score-0').textContent = '0';
document.getElementById('score-1').textContent = '0';
document.getElementById('current-0').textContent = '0';
document.getElementById('current-1').textContent = '0';


// selecting the roll dice button
// this uses an anonymous function
document.querySelector('.btn-roll').addEventListener('click', function() {
    //1. random number
    // the dice will be a random number and will use the Math object
    var dice = Math.floor(Math.random() * 6) + 1;

    //2. display the result - the dice is hidden so it needs to be made visible again
    // variable used to reduce the number of times we call the queryselect on the dice
    var diceDOM = document.querySelector('.dice');
    diceDOM.style.display = 'block';
    diceDOM.src = "dice-" + dice + ".png";
    

    //3. Update the round score if the rolled number was NOT a 1
    if (dice !== 1) {
        // add to the score
        roundScore += dice;
        // display the round score in the UI for the active player
        document.querySelector("#current-" + activePlayer).textContent = roundScore;
    }
    else {
        // change player as the other player rolled 1
        activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;        
        roundScore = 0;
        document.getElementById('current-0').textContent = '0';
        document.getElementById('current-1').textContent = '0';
        // document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
        // document.querySelector('.player-' + activePlayer +'-panel').classList.add('active');
        // now with add or remove, it's difficult to figure out who gets removed and added
        // so instead of adding and removing, toggle can be used to switch the value
        // if a class is active, it becomes inactive and if its inactive it becomes active
        document.querySelector('.player-0-panel').classList.toggle('active');
        document.querySelector('.player-1-panel').classList.toggle('active');
        
        // once a 1 is hit then the dice is hidden again
        diceDOM.style.display = 'none';
    }
});
