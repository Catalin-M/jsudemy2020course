/**
 * Julia and kate are doing a study on dogs. Each of them asked 5 dog owners 
 * about their dog's age, and stored data in an array. We're interested in 
 * knowing if a dog is an adult or a puppy. A dog is an adult if its at least
 * 3 years old and its a puppy if its less than that.
 * 
 * Create a function 'checkDogs' that accepts 2 arrays of dog ages, 'dogsJulia'
 * and 'dogsKate' and does the following:
 * 
 * 1. Julia found out that owners of the first and last two dogs actually have
 * cats and not dogs. Create a shallow copy of the array and remove the cat 
 * ages from those 
 * 2. Create an array with both Julia's correct and Kate's data
 * 3. For each remaining dog log whether its an adult or a puppy ("Dog number 1
 * is an adult and is 5 years old" or "Dog number 2 is still a puppeh!")
 * 
 * Run the above for both test data sets:
 * 1: Julia [3, 5, 2, 12, 7]; Kate [4, 1, 15, 8, 3]
 * 2: Julia [9, 16, 6, 8, 3]; Kate[10, 5, 6, 1, 4]
 */

 "use strict";
 function checkDogs(dogAgeArray1, dogAgeArray2) {
  const shallowDogArray = dogAgeArray1.slice(2, dogAgeArray1.length-1);
  const allDogAges = shallowDogArray.concat(dogAgeArray2);
  console.log(shallowDogArray);
  allDogAges.forEach(function(v, i) {
    if (v < 3) {
      console.log(`Dog number ${i+1} is still a puppeh!`)
    } else {
      console.log(`Dog number ${i+1} is an adult and is ${v} years old!`);
    }
  });
 };

 const juliaArray1 = [3, 5, 2, 12, 7];
 const juliaArray2 = [9, 16, 6, 8, 3];
 const kateArray1 = [4, 1, 15, 8, 3];
 const kateArray2 = [10, 5, 6, 1, 4];

 checkDogs(juliaArray1, kateArray1);
 checkDogs(juliaArray2, kateArray2);