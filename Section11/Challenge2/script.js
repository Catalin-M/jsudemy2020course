/**
 * Create a funct calcAverageHumanAge which accepts an array of dogs ages 
 * (`ages`) and does the following in order:
 * 
 * 1. Calc dog age in human years using the formula:
 * if dog <= 2 then humanAge = 2 * dogAge
 * else if dog > 2 then humanAge = 16 + dogAge * 4
 * 
 * 2. Exclude all dogs less than 18 human years 
 * 
 * 3. Calc average human age of all adult dogs
 * 
 * 4. Run the func for both these data sets:
 * 
 * Test Dt 1: [5, 2, 4, 1, 15, 8, 3]
 * Test Dt 2: [16, 6, 10, 5, 6, 1, 4]
 */
"use strict";
let dogAgesArray1 = [5, 2, 4, 1, 15, 8, 3];
let dogAgesArray2 = [16, 6, 10, 5, 6, 1, 4];

// create an array of human ages
let humanAgeArray1 = dogAgesArray1.map(value => {
  if (value <= 2)
    return value = 0;
  else if (value > 2)
    return value = 16 + value * 4;
});

console.log(humanAgeArray1);

const calcAverageHumanAge = ((accumulator, ageValue, index, array) => {
  if (ageValue <= 2)
    if (ageValue * 2 >= 18)
      accumulator += ageValue * 2;
  else
    accumulator += ageValue * 4 + 16
  return accumulator / array.length;
});


console.log(dogAgesArray1.reduce(calcAverageHumanAge, 0));