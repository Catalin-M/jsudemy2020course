"use strict";

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: "Jonas Schmedtmann",
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: "Jessica Davis",
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: "Steven Thomas Williams",
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: "Sarah Smith",
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector(".welcome");
const labelDate = document.querySelector(".date");
const labelBalance = document.querySelector(".balance__value");
const labelSumIn = document.querySelector(".summary__value--in");
const labelSumOut = document.querySelector(".summary__value--out");
const labelSumInterest = document.querySelector(".summary__value--interest");
const labelTimer = document.querySelector(".timer");

const containerApp = document.querySelector(".app");
const containerMovements = document.querySelector(".movements");

const btnLogin = document.querySelector(".login__btn");
const btnTransfer = document.querySelector(".form__btn--transfer");
const btnLoan = document.querySelector(".form__btn--loan");
const btnClose = document.querySelector(".form__btn--close");
const btnSort = document.querySelector(".btn--sort");

const inputLoginUsername = document.querySelector(".login__input--user");
const inputLoginPin = document.querySelector(".login__input--pin");
const inputTransferTo = document.querySelector(".form__input--to");
const inputTransferAmount = document.querySelector(".form__input--amount");
const inputLoanAmount = document.querySelector(".form__input--loan-amount");
const inputCloseUsername = document.querySelector(".form__input--user");
const inputClosePin = document.querySelector(".form__input--pin");

const calcPrintBal = function (movements) {
  const balance = movements.reduce(
    (accumulator, transaction) => accumulator + transaction, 0);
  // we now update the balance value label
  // its selected above
  labelBalance.textContent = `${balance} GBP`;
};

calcPrintBal(account1.movements);

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

/////////////////////////////////////////////////

let arr1 = ["a", "b", "c", "d", "e"];

// the slice method allows accessing a portion of the array
console.log(arr1.slice(2));  // returns a new array starting at 2 until the end
console.log(arr1.slice(2, 4)); // this is really index 2 and index 3!
console.log(arr1.slice(-2));

// the splice method changes the original array
console.log(arr1.splice(2));  // splice REMOVES elements from the array [c,d,e]
console.log(arr1);  // the original array LOSES the parts extracted [a,b]
// a common use case is removing the last element of the array
console.log(arr1.splice(-1));

// the REVERSE method returns the reversed array
let arr2 = [1, 2, 3, 4, 5];
console.log(arr2.reverse());
console.log(arr2);  // reverse CHANGES the original array!

// CONCAT - concatenates two arrays
let lettersAndNumbers = arr1.concat(arr2);
console.log(lettersAndNumbers); // the new array has a string and ints
console.log([...arr1, ...arr2]);  // does the same as concat!

// JOIN - behaves similarly to join on strings
console.log(arr2.join(" = "));  // the output here is 5 = 4 = 3 = 2 = 1


// the values below are deposits or withdrawals from a bank account
const accMovements = [200, 450, -400, 3000, -650, -130, 70, 1300];

for (let movementOfMoney of accMovements) {
  if (movementOfMoney < 0) {
    console.log(`You widthdrew ${movementOfMoney} GBP!`);
  } else {
    console.log(`You deposited ${movementOfMoney} GBP!`);
  }
}

// the same as above but with forEach
// forEach is a higher order function and it requires a callback function
// forEach calls the function at each element passing in the current element
// as an argument to the callback function!
// forEach passes in the element, the index and the whole array, in that order
accMovements.forEach(function (cashMoney, moneyIndex, moneyArray) {
  if (cashMoney < 0) {
    console.log(`Movement ${moneyIndex + 1}: You widthdrew ${cashMoney} ¬_¬`);
  } else {
    console.log(`Movement ${moneyIndex + 1}: You deposited ${cashMoney} ^_^`);
  }
});

//= Maps and Sets ============================================================//
const currencies = new Map([
  ["USD", "United States dollar"],
  ["EUR", "Euro"],
  ["GBP", "Pound sterling"],
]);

// forEach works similarly with Maps as it does with arrays
currencies.forEach(function (value, key, currencies) {
  console.log(`The key: ${key}; the value: ${value}`);
});

// the keys inside a SET are the same as the values
let uniqueCurrencies = new Set(["GBP", "USD", "HUF", "EUR", "RON"]);
uniqueCurrencies.forEach(function (value, key, uniqueCurrencies) {
  console.log(`The key: ${key}; the value: ${value}`);
});

// ========================= back to dom manipulations =======================//
// this code will make the content of the movements arrays inside each account
const displayMovements = function (transactions) {
  //the container of the transactions should be cleared of contents
  // and only after that should new entries be added
  // containerMovements was already selected with querySelector further up
  containerMovements.innerHTML = "";

  transactions.forEach(function (transaction, index) {
    // variable which will say if we're depositing or widthdrawing
    const transactionType = transaction > 0 ? "deposit" : "withdrawal";

    const htmlTemplate = `<div class="movements__row">
    <div class="movements__type movements__type--${transactionType}">${index + 1}</div>
    <div class="movements__date">3 days ago</div>
    <div class="movements__value">${transaction}</div>
    </div>`;

    // we select the container to which we will be adding a new row
    // the first argument is the position relative to the element
    // the second parameter is the text to be added
    containerMovements.insertAdjacentHTML("afterbegin", htmlTemplate);
  });
};
displayMovements(account1.movements);

// = the Map metod ===========================================================//
// the map method gives a new array; it works similarly to forEach

let eurToUsdMovements = [5000, 3400, -150, -790, -3210, -1000, 8500, -30];
const exchangeRate = 1.1;

// the map method takes a callback function and an argument 
// the argument is each individual element inside the original array
// here, each element is taken and multiplied by a value and these new values
// are saved inside a new array
let usdMovements = eurToUsdMovements.map(movement => movement * exchangeRate);
console.log(eurToUsdMovements);
console.log(usdMovements);

let transactionDescriptions = eurToUsdMovements.map((element, index, array) => {
  if (element > 0) {
    return `Transaction ${index + 1}: You deposited ${element}`;
  } else {
    return `Transaction ${index + 1}: You widthdrew ${Math.abs(element)}`;
  }
});
console.log(transactionDescriptions);

// generating the username
// here a list of account items is received
// a new property username is added to each individual account 
// which is build off of the owner property
const createUsernames = function (userAccounts) {
  userAccounts.forEach(function (acc) {
    acc.username = acc.owner
      .toLowerCase()
      .split(" ")
      .map(name => {
        return name[0];
      }).join("");
  });
};

createUsernames(accounts);

console.log(accounts);

// = Filter method - it filters an array for an element that fulfills a 
// = certain condition// the below only returns positive transactions
const deposits = movements.filter(function (transaction) {
  return transaction > 0;
});

console.log(deposits);

// = The Reduce Method =======================================================//

const balance = movements.reduce(function (accumulator, currValue, theArra) {
  // add the current value to the accumulator
  return accumulator + currValue;
}, 0);

// and with arrow functions

const bal2 = movements.reduce((acc, currVal) => acc + currVal, 0);
console.log("The accumulator: " + balance);

// get maximum value using the reduce method  
let maxValue = movements.reduce(
  (accumulator, transaction) => {
    if (transaction > accumulator) 
      return transaction;
    else
      return accumulator; 
  }, movements[0]);
console.log("The max value is: " + maxValue);
