const testMap = new Map();
testMap.set("Name", "Downtown Internet Caffee");
testMap.set(1, "Dota 2");
testMap.set(2, "CS 1.6");

testMap.set(3, "Warcraft 3")
  .set(4, "ciggarettes")
  .set(5, "beer");

// ^ elements of a good time ^_^
console.log(testMap);

console.log(testMap.has(2));
//testMap.delete(1);

testMap.set([1, 2], "This cannot be reached O_O");
testMap.set(document.querySelector("h1"), "The selected H1 element ^_^");

console.log(testMap);

//creating a map

const question = new Map([
  ["question", "What is the best game in the world?"],
  [1, "Dota 2"],
  [2, "CS 1.6"],
  [3, "Quake 3 Arena"],
  [4, "Don't play games, it's a waste of time!"],
  ["correct", 1]
]);

console.log(question);

//convert object to map!
const games = {
  rts: "Starcraft Brood War",
  moba: "Dota 2",
  fps: "Quake Champions",
  rpg: "Path of Exile"
}

const gamesMap = new Map(Object.entries(games));
console.log(gamesMap);
console.log(games);

//iterating through the elements of a map
console.log(question.get("question"));
for (const [key, value] of question) {
  // check if the key is a number
  if (typeof (key) === 'number')
    console.log(`Answer ${key}: ${value}`);
}

// the value of the answer is saved in the yourAnswer variable
// this is a string so it gets parsed  to an Int and then if it's the correct 
// number, then you get a cookie, otherwise you get called a noob!
const yourAnswer = prompt("Your answer: ");
parseInt(yourAnswer) === question.get("correct") ?
  console.log("Cookies for you") :
  console.log("You noob!");