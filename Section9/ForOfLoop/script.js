const menu = ["Pizza", "Soup", "Tea"];

for (let food of menu) {
  console.log(food);
}

// because the index cannot be accessed easily, we need to access the entries
for (let item of menu.entries()) {
  console.log(item);
}

// pre ES6 to add a smaller object into a large one
// there would be an assignment of sorts within the larger object for ex.
// smallObject: smallObject
const smallObject = {
  programmingLanguages: ["Java", "JavaScript", "C#"],
  interests: {
    coding: ["Java", "C#"],
    games: {
      dota2: "Dota 2",
      afps: ["Diabotical", "Quake Champions"],
      rts: ["Brood War", "Wings of Liberty", "Age of Empires"]
    }
  }
};

const largeObject = {
  name: "pSYoniK",
  age: 69,
  weight: "Wouldn't you like to know!",
  height: 182,
  // Old way ---
  // smallObject: smallObject
  // New Way
  smallObject,

  // Old way ---
  oldOrder: function (something) {
    console.log(something);
  },

  // New way ---
  newOrder(somethingElse) {
    console.log(somethingElse);
  }
};

largeObject.oldOrder("cat");
largeObject.newOrder("dog");

console.log(largeObject);

// Optional chaining allows returning Undefined instead of throwing an error
// if a parameter is missing or there is no value on a property
console.log(largeObject.smallObject.interests?.gaming);

// the Object.keys() method returns an array of a given object's 
// enumerable property names 
for (let game of Object.keys(smallObject.interests.games)) {
  console.log(game);
}

// to get the property values, Object.values() is used
for (let titles of Object.values(smallObject.interests.games)) {
  console.log(titles);
}

// to iterate through the object itself
const entries = Object.entries(smallObject);
console.log(entries);

for (const [key, value] of entries) {
  console.log(`On ${key} we have the ${value} value.`);
}