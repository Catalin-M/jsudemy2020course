/**
 * There is a map with a series of events
 * The values are the events themselves and the keys are the minutes at which
 * the events occured
 * 
 * 1. Create an array "events" of the different events that happened without
 * any duplicates
 * 2. After the game, the yellow card from minute 64 was unfair so it needs to 
 * be removed
 * 3. Print the following - an event happend on average every X minutes
 * 4. Loop over the events and log them on the console, marking whether its 
 * in the first half (<45) or the second half like this:
 * [First half] 17: GOAL
 */

const gameEvents = new Map([
  [17, "Goal"],
  [36, "Substitution"],
  [47, "Goal"],
  [61, "Substitution"],
  [64, "Yellow card"],
  [69, "Red Card"],
  [70, "Substitution"],
  [71, "Substitution"],
  [76, "Goal"],
  [80, "Goal"],
  [92, "Yellow Card"]
]);

// adding all the values into a set; since a set doesn't allow duplicates
// the output will contain only unique values
let uniqueEvents = new Set();
for (let theValues of [...gameEvents.values()]) {
  console.log(theValues);
  uniqueEvents.add(theValues);
}

// the unfair yellow card can be simply deleted
gameEvents.delete(64, "Yellow card");

console.log(`An event happened every ${90 / gameEvents.size} minutes`);

for (let [key, value] of gameEvents) {
  key < 45 ? console.log(`[First Half] ${key}: ${value}`) :
    console.log(`[Second Half] ${key}: ${value}`);
}