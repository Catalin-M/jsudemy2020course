// Coding challenge

/**
 * This is the basis of a football betting app
 *
 * Suppose we get data from a web service about a certain game. In this
 * challenge we're going to work with the data:
 *
 * 1. Create one player array for each team. variables `players1` and `players2`
 * 2. The first player in any player array is the goalkeeper and the others
 * are field players. For Bayern Munic (team 1) create one variable (`gk`)
 * with the goalkeepers name, and one array (`fieldPlayers`) with all the re-
 * maining 10 players
 * 3. Create an array `allPlayers` containing all players for both teams (22
 * players)
 * 4. During the game, Bayern Munic (team 1) used 3 substitute players, so
 * create a new array `playersFinal containing all the original team 1 players
 * plus `Thiago`, `Coutinho` and `Perisic`
 * 5. Based on the game.odds object create one variable for each odd called
 * `team 1`, `draw` or `team 2`
 * 6. Write a function `printGoals` that receives an arbitrary number of player
 * names (not an array!) and prints each of them to the console, along with the
 * number of goals who were scored (number of player names passed in)
 * 7. The team with the lower odd is more likely to win. Print to the console
 * which team is likelier to win, WITHOUT using an if/else statement or the
 * ternary operator
 */

const game = {
  team1: "Bayern Munich",
  team2: "Borrussia Dortmund",
  players: [
    [
      "Neuer",
      "Pavard",
      "Martinez",
      "Alaba",
      "Davies",
      "Kimmich",
      "Goretzka",
      "Coman",
      "Muller",
      "Gnarby",
      "Lewandowski"
    ],
    [
      "Burki",
      "Schulz",
      "Hummels",
      "Akanji",
      "Hakimi",
      "Weigl",
      "Witsel",
      "Hazard",
      "Brandt",
      "Sancho",
      "Gotze"
    ]
  ],
  score: "4:0",
  scored: [
    "Lewandowski", "Gnarby", "Lewandowski", "Hummels"
  ],
  date: "Nov 9th, 2037",
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5
  }
};

function likelierToWin(...odds) {
  console.log(Math.min(...odds));
}

// logic approach to highlight the likelier team to win
function likelikerToWinLogic(...odds) {
  odds[0] < odds[2] && console.log("Team 1 is likelier to win!");
  odds[0] > odds[2] && console.log("Team 2 is likelier to win!");
}

function printGoals(...playerNames) {
  console.log(`${playerNames.length} goals were scored`);
  for (let i = 0; i < playerNames.length; i++) {
    console.log(playerNames[i]);
  }
}

// the first task is destructuring arrays; players is part of an object
// but it is an array with 2 elements each of which is an individual array
const [players1, players2] = game.players;

// the players1 array gets destructured into 2 values
// gk is a variable which will get the first value from the players1 array
// and fieldPlayers is a rest param that will hold the rest of the variables
const [gk, ...fieldPlayers] = players1;

// destructure the 2 player arrays into individual variables
const allPlayers = [...players1, ...players2];

// new array that holds all elements in players2 + 3 additional players
const playersFinal = [...players1, "Thiago", "Coutinho", "Perisic"];

// this is the easier way of accessing the property itself
// const { team1, x: draw, team2 } = game.odds;
// the destructuring can also be done using nested properties
const { odds: { team1, x: draw, team2 } } = game;

// destructure the array so that player names are passed in
printGoals(...game.scored);

// pass in the 3 variables
likelierToWin(team1, draw, team2);

likelikerToWinLogic(team1, draw, team2);