'use strict';

const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]]
  },

  orderDelivery: function ({ starterIndex, mainIndex, time, address }) {
    console.log(`Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`);
  },

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    }
  },

  orderPasta: function (ingredient1, ingredient2, ingredient3) {
    console.log(`Here is your delicious pasta with ${ingredient1}, ${ingredient2} and ${ingredient3}`);
  }
};

restaurant.orderDelivery({
  starterIndex: 2,
  mainIndex: 2,
  time: "22:30",
  address: "Aleea Narciselor"
});

// destructuring the array arr into variables x, y, z
// same as going const x = arr[0]; const y = arr[1]; etc
const arr = [1, 2, 3];
const [x, y, z] = arr;
console.log(x);

const [first, , third] = restaurant.categories;
console.log(first, third);

const [starter, main] = restaurant.order(2, 1);
console.log(starter, main);

// how do we deal with nested arrays?
const nested = [2, 4, [5, 6]];
const [i, , j] = nested;
// j now represents an array
// but to access each individual element, we can destructure 
// inside a destructure
const [element1, , [arrayElement1, arrayElement2]] = nested;
console.log(element1, arrayElement1, arrayElement2);

// default values in destructuring arrays
// let us assume w edo not known the array size
const [p = 1, q = 1, r = 1] = [8, 9];
console.log(p, q, r);

// objects can also be destructured
// the exact property name needs to be written to extract the values from 
// the object
const { name, openingHours, categories } = restaurant;
console.log(name, openingHours, categories);

// providing a new name for each of the properties is done with :
const {
  name: restaurantName,
  openingHours: hours,
  categories: tags
} = restaurant;
console.log(restaurantName, hours, tags);

// mutating variables
let a = 111;
let b = 999;
const obj1 = {
  a: 23,
  b: 7,
  c: 14
};

// we cannot use const {a, b} since those are already declared
// const {a, b} = obj1; cannot be used
// to be able to reassign the values of a and b the following format is used
({ a, b } = obj1);
console.log(a, b);

// nested objects
const { fri } = openingHours;
console.log(fri);
// this will output {open: 11, close: 23}
const { fri: { open, close } } = openingHours;
console.log(open, close);

// the spread operator
// if I want to access the values inside an array, I need to destructure the 
// values inside of it or I need to access each field with its index
const arr1 = [4, 5, 6];
const badArray = [2, 3, arr1[0], arr1[1], arr1[2]];

// this isn't very neat, so to overcome this and simplify the process
// the spread operator (...) can be used to access the individual elements
// inside a given array
const goodWay = [2, 3, ...arr1];

// the two arrays will be identical, but its simple to see the second variant
// is less verbose and easier to read
console.log(badArray);
console.log(goodWay);

// the spead can be used to add elements to an existing array easily taken
// from the object
const newMenu = [...restaurant.mainMenu, 'Gnocci'];
console.log(newMenu);

// Copy array
const mainMenuCopy = [...restaurant.mainMenu];

// join 2 arrays 
const largeMenu = [...restaurant.starterMenu, ...restaurant.mainMenu];
console.log(largeMenu);

// ex of iterable
const myName = "pSYoniK";
const myNamesLetters = [...myName];
console.log(myNamesLetters);  // this shows an array
console.log(...myName);       // this just shows the individual values

// ex of passing through an array to a function that expects 3 variables
const ingredients = ['Pasta', 'Minced meat', 'Parmeggiano'];
restaurant.orderPasta(...ingredients);

// instead of usingthe hardcoded values, a prompt can also be used
let promptedIngredients = [prompt("Let us make pasta! Ingredient 1:"),
prompt("Ingredient 2:"), prompt("Ingredient 3:")];

restaurant.orderPasta(...promptedIngredients);

// copy object and add a few more fields!
const newRestaurant = {
  ...restaurant,
  founder: 'Mario',
  founded: 1998,
  bankcrupt: 1999
};

console.log(newRestaurant);

//spread operator
const arrSpread = [1, 2, ...[3, 4, 5]];

//similar to destructuring
// the rest operator
const [ax, bx, ...restOf] = [1, 2, 3, 4, 5];

console.log(arrSpread);
console.log(ax, bx, restOf);

const [pizza, , rissotto, ...otherFood] = [...restaurant.mainMenu,
...restaurant.starterMenu];
console.log(pizza, rissotto, otherFood);

// destructure into an object that holds a single day 
// and into another object that contains two objects
const { sat, ...weekdays } = restaurant.openingHours;
console.log(sat, weekdays);

// using rest parameter to add whatever number of variables together
// and return their sum 
/**
 * add(2,3) - sould return 5
 * add(3, 4, 5) - should return 12
 * add(....) - return whatever the sum of ALL the elements is
 */

const add = function (...numbers) {
  let sum = 0;
  for (let i = 0; i < numbers.length; i++) {
    sum += numbers[i];
  }
  return sum;
}

// whatever number of elements is passed in gets converted to an array
console.log(add(3, 4, 5, 623847, 132, 2));

// we can also pass in an array  that we SPREAD 
const listOfNumbers = [1, 2, 3, 4, 5];
console.log(add(...listOfNumbers));

function orderFancyPizza(mainIngredient, ...randomIngredients) {
  console.log(mainIngredient, randomIngredients);
}

orderFancyPizza("salami", "mushrooms", "peppers", "cheese");