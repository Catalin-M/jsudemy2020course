/**
 * 1. Loop over the game.scored array and print each player name to the console,
 * along with the goal number (EX: Goal 1: Lewandowski)
 * 2. Use a loop to calculate the average odd and log it to the console
 * 3. Print the 3 odds to the console but in a formatted way:
 *  Odd of victory Bayern Munich: 1.33
 *  Odd of draw: 3.25
 *  Odd of victory Borrussia Dortmund: 6.5
 * Get the team names from the game object, don't hardcode them. 
 * Create an object called "scorers" which contains the names of the players who
 * scored as properties, and the number of goals as the value. In this game, it
 * will look like this:
 * {
 *  Gnarby: 1,
 *  Hummels: 1,
 *  Lewandowski: 2
 * }
 */

const game = {
  team1: "Bayern Munich",
  team2: "Borrussia Dortmund",
  players: [
    [
      "Neuer",
      "Pavard",
      "Martinez",
      "Alaba",
      "Davies",
      "Kimmich",
      "Goretzka",
      "Coman",
      "Muller",
      "Gnarby",
      "Lewandowski"
    ],
    [
      "Burki",
      "Schulz",
      "Hummels",
      "Akanji",
      "Hakimi",
      "Weigl",
      "Witsel",
      "Hazard",
      "Brandt",
      "Sancho",
      "Gotze"
    ]
  ],
  score: "4:0",
  scored: [
    "Lewandowski", "Gnarby", "Lewandowski", "Hummels","Gnarby","Gnarby","Gnarby"
  ],
  date: "Nov 9th, 2037",
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5
  }
};

// printing each goal and the name of the player who scored it
// entries are key value pairs, but they start at 0, so incrementing the values
// before print will output the expected results
// ---> destructuring could also be used instead of using indices
let goals = Object.entries(game.scored);
for (let goal of goals) {
  goal[0]++;
  console.log(`Goal ${goal[0]} scored by ${goal[1]}`);
}

// going through the values inside the game.odds property which retrieves the
// correct value of each of the odds; this is then divided by 3, as there are 
// 3 different odds
let sum = 0;
for (let odd of Object.values(game.odds)) {
  sum += odd;
}
console.log("The average odd is: " + sum / 3);

// printing the odds for each team
for (let [teamName, odd] of Object.entries(game.odds)) {
  // user ternary operator to change the text needed to print out
  const calloutValue = teamName === "x" ? "draw" : `victory ${game[teamName]}`;
  console.log(`Odd of ${calloutValue}: ${odd}`);
}

/* Create an object called "scorers" which contains the names of the players who
 * scored as properties, and the number of goals as the value. In this game, it
 * will look like this:
 * {
 *  Gnarby: 1,
 *  Hummels: 1,
 *  Lewandowski: 2
 * }
 * */
let scorers = {};

// create an object and then if that property is null, it means I didn't add that
// player to the list, otherwise, if the player is already there, it means they
// already scored and that score neesd to be increased
for (let player of game.scored) {
  if (scorers[player] == null) {
    scorers[player] = 1;
  } else {
    scorers[player]++;
  }
}
console.log(scorers);