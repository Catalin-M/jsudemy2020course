const orderSet = new Set([
  "beer",
  "vodka",
  "wine",
  "beer"
]);

console.log(orderSet);
console.log(orderSet.size);
console.log(orderSet.has("beer"));

orderSet.add("gin & tonic");
orderSet.delete("wine");

const randomArray = [1, 2, 3, 4, 5, 5, 5, 6];
const cleanSet = new Set(randomArray);
const cleanArray = [...new Set(randomArray)];

console.log(cleanSet);
console.log(cleanArray);