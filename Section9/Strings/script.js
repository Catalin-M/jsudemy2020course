const airline = "Ciuc Air Harghita";
const plane = "A320neo";

console.log(plane[0]);
console.log("Dota2"[0]);
console.log(airline.length);

console.log(plane.indexOf(2));
console.log(airline.lastIndexOf("i"));

// getting a substring starting from a given index
console.log(plane.slice(1));

//get the first word in the string
console.log(airline.slice(0, airline.indexOf(' ')));

// counts from the end of the string - returns "eo"
console.log(plane.slice(-2));

//function that returns if a given seat is a middle seat
const checkMiddleSeat = function (seat) {
  // B and E are middle seats
  if (seat.slice(-1) === "E" || seat.slice(1) === "B") {
    console.log("You got the middle seat");
  }
  else {
    console.log("You got the aisle/window seat!");
  }
}

checkMiddleSeat("23E");
checkMiddleSeat("11A");
checkMiddleSeat("6B");

// you can capitalize all the characters or make them small with these methods
console.log(airline.toLowerCase());
console.log(airline.toUpperCase());

// comparing emails
const email = "psyonik@amazing.com";
const loginEmail = "  pSYoniK@amazinG.com \t";
// the above are both realistically the same 
// the characters are the same and the difference is only space, tabbing and
// capitalisation

const normalisedEmail = loginEmail.toLowerCase().trim();
console.log(normalisedEmail === email);

// replacement - 
const price = "$1000";
const slashPrice = price.replace("$", "#");
console.log(slashPrice);

const fox = "the quick brown fox jumps over the lazy dog";
// replace all occurences of "the"
console.log(fox.replace(/the/g, "O_O")); // this replaces the with O_O
console.log(fox.includes("the"));       // true
console.log(fox.startsWith("THE"));     // false

const checkBackage = function (bagContents) {
  (bagContents.toLowerCase().includes("gun")
    || bagContents.toLowerCase().includes("knives"))
    ? console.log("You can't get on board!")
    : console.log("Welcome aboard!");
}

checkBackage("I got some socks, food and knives in my bags");
checkBackage("I have socks, tshirt and laptop");
checkBackage("I have a gun, socks and some knives");

// a given string can also be split up based on a given divider character
console.log("pSYoniK+gHOst+is+really+GOOD!".split("+"));  // outputs array

// an array can also be joined together
// join will need a parameter otherwise it uses ","
console.log(["The", "quick", "brown", "fox"].join(" "));

let capitalizedName = [];
const longName = "jim dikson von somwhere";
for (let n of longName.split(" ")) {
  capitalizedName.push(n.replace(n[0], n[0].toUpperCase()));
}
console.log(capitalizedName);

// padding a string
const message = "You got an office!";
// padStart() pads from the start of the string up until the total string legnth
// reaches that padding value - below, the string is padded with "+___" until 
// the total string length reaches 36!
console.log(message.padStart(36, "+___"));
// a more real world example of padding
const cardNumber = "1234567812345678";
let newNumber = cardNumber.slice(-4); // grab last 4 digits
console.log(newNumber.padStart(cardNumber.length, "*"));

// repeating strings
const stringToRepeat = "Go middle!";
console.log(stringToRepeat.repeat(5));