"use strict";

// select element by its id
const score0EL = document.querySelector("#score--0");
const score1EL = document.querySelector("#score--1");
// select the dice
const diceEL = document.querySelector(".dice");
// selecting the buttons
const btnNEW = document.querySelector(".btn--new");
const btnROLL = document.querySelector(".btn--roll");
const btnHOLD = document.querySelector(".btn--hold");
// selecting the player scores - this is no longer used
const player1Score = document.querySelector("#current--0");
const player2Score = document.querySelector("#current--1");
// selecting the player sections
const player1Section = document.querySelector(".player--0");
const player2Section = document.querySelector(".player--1");
let currentScore;
let scores;
let playing;
let activePlayer;

// initializes everything and also resets the winner class 
function init() {
  // holding the game currentScore per round
  currentScore = 0;
  // holding the large, overall scores - p1 is position 0 and p2 is position 1
  scores = [0, 0];
  // this will hold information to confirm the game is still active
  playing = true;
  score0EL.textContent = 0;
  score1EL.textContent = 0;
  player1Score.textContent = 0;
  player2Score.textContent = 0;
  // variable to hold the active player - 0 is player 1 and 1 is player 2 (O_O)
  activePlayer = 0;
  // this will hide the dice image
  diceEL.classList.add("hidden");
  btnHOLD.classList.remove("hidden");
  btnROLL.classList.remove("hidden");
  document.querySelector(`.player--0`).classList.remove("player--winner");
  document.querySelector(`.player--1`).classList.remove("player--winner");
  document.querySelector(".player--0").classList.add("player--active");
}

init();

// function to switch the player
const switchPlayer = function () {
  // switch to next player if 1 is rolled
  // if the active player is 0 then switch to 1 else switch to 0
  document.getElementById(`current--${activePlayer}`).textContent = 0;
  // the toggle method adds the element if its not there or removes it
  // if its there
  currentScore = 0;
  activePlayer = activePlayer === 0 ? 1 : 0;
  player1Section.classList.toggle("player--active");
  player2Section.classList.toggle("player--active");
};

//rolling the dice functionallity
btnROLL.addEventListener("click", function () {
  if (playing) {
    // when the button is pressed, a random dice roll needs to be generated
    const diceRollValue = Math.floor(Math.random() * 6) + 1;

    // display the dice by using a template literal to fill in the filename
    // with the randomly generated number
    diceEL.src = `dice-${diceRollValue}.png`;
    diceEL.classList.remove("hidden");

    // check if '1' is rolled and if its true switch to next player
    if (diceRollValue !== 1) {
      currentScore += diceRollValue;
      document.getElementById(
        `current--${activePlayer}`
      ).textContent = currentScore;
      // player1Score.textContent = currentScore; --- no longer needed
    } else {
      switchPlayer();
    }
  }
});


// the hold button configuration
btnHOLD.addEventListener("click", function () {
  if (playing) {
    // 1. add the current currentScore to the overall scores
    scores[activePlayer] += currentScore;
    document.querySelector(`#score--${activePlayer}`).textContent =
      scores[activePlayer];

    // 2. check if the currentScore is over 100 and then mark player as winner
    if (scores[activePlayer] >= 190) {
      // the game finishes
      playing = false;
      // assign player--winner class
      document.querySelector(`.player--${activePlayer}`)
        .classList.add("player--winner");
      document.querySelector(`.player--${activePlayer}`)
        .classList.remove("player--active");

      // hide the buttons to roll, hold and the dice
      btnHOLD.classList.add("hidden");
      btnROLL.classList.add("hidden");
      diceEL.classList.add("hidden");
    } else {
      // 3. switch player if its not over 100
      switchPlayer();
    }
  }
});

// this will reinitialize the whole screen, reset all the values
// and make the roll/hold buttons visible again after the game finishes
btnNEW.addEventListener("click", function () {
  init();
});

/*
 * the above could also have a signature as ("click", init);
 * at which point JS would call it upon reaching the code
 */