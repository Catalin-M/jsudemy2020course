"use strict";

const modal = document.querySelector(".modal"); // the var holds the modal
const overlay = document.querySelector(".overlay");
const btnCloseModal = document.querySelector(".close-modal");
const btnShowModal = document.querySelectorAll(".show-modal");

const closeMod = function closeModal() {
  modal.classList.add("hidden");
  overlay.classList.add("hidden");
};

const openMod = function openModal() {
  modal.classList.remove("hidden");
  overlay.classList.remove("hidden");
};

// iterate through buttons to either open the modal
// or close the modal by clicking outside of it or on its close button
for (let i = 0; i < btnShowModal.length; i++) {
  btnShowModal[i].addEventListener("click", openMod);
}
// if the close button is pressed, then close the modal
btnCloseModal.addEventListener("click", closeMod);
// if the blurred overlay is clicked, then closet he modal
overlay.addEventListener("click", closeMod);

// listen for keypresses of the esc key
// the function receives the event as an argument
// because the function isn't called by us, JS calls it!
document.addEventListener("keydown", function (e) {
  if (e.key === "Escape" && !modal.classList.contains("hidden")) {
    // here the function needs to be called explicitly as JS doesn't call it
    closeMod();
  }
});
