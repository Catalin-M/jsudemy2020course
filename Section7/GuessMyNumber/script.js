"use strict";

// defining a random number between 1 and 20
// that will be the 'secret number'
let secretNumber = Math.floor(Math.random() * 20) + 1;
// this will be the score for a given round
let score = 20;
// this will be the variable holding the highscore
let highScore = 0;

// the check button gets to handle a number of events when its pressed
document.querySelector(".check").addEventListener("click", function () {
  // the value in the check input field, gets stored
  // however, the value is retrieved as a string so it needs to be converted
  // to a number
  let guess = Number(document.querySelector(".guess").value);

  // the first check is to see if the guess is empty/false/0/nan
  // guess would be FALSE (0) if there is no input, so it gets turned into
  // true and the below code is executed
  if (!guess) {
    document.querySelector(".message").textContent = "No number entered!";
  }
  // if the guess is correct
  else if (guess === secretNumber) {
    document.querySelector(".message").textContent = "Correct number guessed!";
    document.querySelector(".number").textContent = guess;
    document.querySelector("body").style.backgroundColor = "#60aa50";
    document.querySelector(".number").style.width = "40rem";
    if (score > highScore) {
      highScore = score;
      document.querySelector(".label-highscore").textContent =
        "🥇 Highscore: " + highScore;
    }
  }
  // if the guess is incorrect
  else if (guess !== secretNumber) {
    if (score > 1) {
      document.querySelector(".message").textContent =
        guess > secretNumber ? "Too high!" : "Too low!";
      score--;
      document.querySelector(".score").textContent = score;
    } else {
      document.querySelector(".score").textContent = 0;
      document.querySelector(".message").textContent = "You lost :(";
    }
  }
});

// upon pressing the again button, the screen should be reset to its default
document.querySelector(".again").addEventListener("click", function () {
  score = 20;
  secretNumber = Math.floor(Math.random() * 20) + 1;
  document.querySelector(".message").textContent = "Start guessing...";
  document.querySelector(".score").textContent = score;
  document.querySelector(".guess").value = "";
  document.querySelector(".number").textContent = "?";
  document.querySelector(".number").style.width = "15rem";
  document.querySelector("body").style.backgroundColor = "#222";
});
