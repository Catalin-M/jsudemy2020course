// this will return 33 and 'Window' because the this keyword
// refers to the context of the call
// calculateAge(1987);

// function calculateAge(year) {
//     console.log(2020-year);
//     console.log(this);
// }

// this refers to an 'Object' with the name John, a dob of 1990
// and a method called calculateAge - this is because the 'this'
// keyword refers to an instance of the object inside of which it is called
var john = {
  name: "John",
  yearOfBirth: 1990,
  calculateAge: function () {
    console.log(this);
    console.log(2020 - this.yearOfBirth);

    function innerFunc() {
      console.log(this);
    }
    innerFunc();
  },
};

john.calculateAge();

// method borrowing allows an object to copy the same function from another object
// through a simple assignment
var mike = {
  name: "Mike",
  yearOfBirth: 1984,
};

// this assignment allows mike to have the same method as john
mike.calculateAge = john.calculateAge;

mike.calculateAge();
