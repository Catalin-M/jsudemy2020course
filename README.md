# JavaScriptUdemy
All the work done through the Javascript course on udemy by Jonas

The repository will be holding all the work I'm doing throughout **The Complete JavaScript Course 2020: Build Real Projects**. 
I am also using the repository as an opportunity to explore branching more in depth in a controlled environment and add any work I do in that project.

The goal is to pickup enough JS to rebuild the front-end of my personal page while rebuilding the backend as well.
