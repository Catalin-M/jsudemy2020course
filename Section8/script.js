"use strict";

function calcAge(birthyear) {
  const age = 2020 - birthyear;

  function printAge() {
    const output = `You are ${age} years old and you were born in ${birthyear}`;
    console.log(output);

    if (birthyear >= 1981 && birthyear <= 1996) {
      const millenial = `Oh, and you're a millenial, ${firstName}`;
      console.log(millenial);
    }
  }
  printAge();
  return age;
}

const firstName = "pSYoniK";
calcAge(1987);

/* the this keyword */
const pSYoniK = {
  name: "pSYoniK",
  year: 1987,
  calculateAge: function () {
    return 2020 - this.year;
  }
};
console.log(pSYoniK.calculateAge());

const ageCalculator = function (yearOfBirth) {
  console.log(2020 - yearOfBirth);
  console.log(this);
}
ageCalculator(1987);

const ageCalculatorArrow = yearOfBirth => {
  console.log(2020 - yearOfBirth);
  console.log(this);
}
ageCalculatorArrow(1987);
