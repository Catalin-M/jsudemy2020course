"use strict";

const bookings = [];

const createBooking = function (flightNum, numPassengers = 1, price = 199) {
  //es5
  // numPassengers = numPassengers || 1; // to assign a default value
  // price = price || 199;
  const booking = {
    flightNum,
    numPassengers,
    price
  }
  console.log(booking);
  bookings.push(booking)
}

createBooking("EZY4290");
createBooking("LHR1234", 59, 187);
createBooking("BHR2298", 240);
createBooking("DBY8799", undefined, 599);

//===========================default parameters===============================//

const flight = "RTL0014";
const pSYoniK = {
  name: "pSYoniK",
  passport: 123456789
}

const checkin = function (flightNumber, passengerObject) {
  // let's assume the flightNumber was changed
  // and it changes the name of the passenger
  flightNumber = "RTL1990";
  passengerObject.name = "Mr. " + passengerObject.name;

  if (passengerObject.passport === 123456789) {
    alert("Check In");
  } else {
    alert("Wrong passport number!");
  }
}

checkin(flight, pSYoniK);
console.log("The flight number after check in: " + flight);
console.log("The passenger object after check in: ");
console.log(pSYoniK);


// function accepts any person and changes the passport number
const newPassport = function (person) {
  person.passport = Math.trunc(Math.random() * 100000000);
}
newPassport(pSYoniK); //here the passport number gets changed
checkin(flight, pSYoniK);     //here this will fail as the passport nr is manipulated 
console.log(pSYoniK);


//= First order and higher order functions ===================================//
const oneWord = function (str) {
  return str.replace(/ /g, "").toLowerCase();
}

const upperFirstWord = function (str) {
  // the incoming string is split so that there is one element and then the 
  // rest are in an array; the first word is converted to uppercase and then the
  // other array gets merged back
  const [first, ...remainingWords] = str.split(" ");
  return [first.toUpperCase(), ...remainingWords].join(" ");
}

// higher order function as it takes in a function as a parameter!
const transformer = function (str, funct) {
  console.log(`The original string ${str}`);
  // here the function is actually CALLED () whereas when its passed in
  // a value is passed in!
  console.log(`Transformer string: ${funct(str)}`);

  console.log(`Transformer by: ${funct.name}`);
}

transformer("Javascript is the best language!", upperFirstWord);

//= Functions returning other functions ======================================//
const greet = function (greeting) {
  return function (name) {
    console.log(`${greeting} from ${name}`);
  }
}

// the value here for greeterHey is a FUNCTION
// now greeterHey can be called as a previously defined function
const greeterHey = greet('Hey');

greeterHey('Jim');
greeterHey('Cici'); // the greeting comes from the inside function!

// we can call them like this as well:
// the first part is a function so the second part is the value passed in to the
// first function!
greet('Hello')('Cheechee');

// wow ... much arrow, such pointing!!!
const greetArrow = greeting => name => console.log(`${greeting} from ${name}`);

greetArrow('Hi')('from Arrow');

//= The call and apply methods ===============================================//

let lufthansa = {
  airline: "Lufthansa",
  iataCode: "LH",
  bookings: [],
  // the below is the easier way of writing
  // book: function() {}
  book(lufthansaFlightNumber, lufthansaPassengerName) {
    console.log(`${lufthansaPassengerName} booked a flight on ${this.airline} with flight ${this.iataCode}${lufthansaFlightNumber}`);

    // this booking is also added to the list of bookings
    this.bookings.push({ flight: `${this.iataCode},${lufthansaFlightNumber},${lufthansaPassengerName}` });
  }
};

lufthansa.book(5776, 'pSYoniK Ghost');
lufthansa.book(123, 'Jimmy Dikins');
console.log(lufthansa.bookings);


// let's assume lufthansa created a new subgroup
let eurowings = {
  airline: "Eurowings",
  iataCode: "EW",
  bookings: []
  // and here we would want to have access to the same book() method
};

// because JS has first class functions, here we can pass on the function to a 
// variable
const book = lufthansa.book;

// if this method is now called, book(23, "Jim Jimmy") won't work as it existed
// only inside the object, it's now a function ON ITS OWN, so the 'this' keyword
// used inside of it WILL Not work
// as such, call is used to point book to the eurowings OBJECT and the function
// acts like a METHOD for the eurowings object
book.call(eurowings, 1337, "Bruce Almighty");
book.call(lufthansa, 7331, "Jane Almighty");

let swiss = {
  airline: "Swiss",
  iataCode: "CH",
  bookings: []
  // and here we would want to have access to the same book() method
};
book.call(swiss, 8008, "Mary Marison");

// the Apply method does something somewhat similar, but it doesn't get the 
// arguments, but expects an array of arguments
const flightData = [8080, "Randy Randerson"];
book.apply(swiss, flightData);
console.log(swiss);

//= The bind method ==========================================================//
// here I will assume that the book method needs to be used ONLY with an airline
// and not have it swap around; it cannot be called and it RETURNS a function!
const ewBook = book.bind(eurowings);

// the method can be used to create methods for each of the airlines
const chBook = book.bind(swiss);
ewBook(7899, "Rando Randersen");  // this is now a method inside eurowings
chBook(8891, "Sweety Pie"); // this is a method inside swiss

// bind can accept additional arguments so here for example, the method below
// only needs the name since the NUMBER was already preset!
const ewBookFlight1337 = book.bind(eurowings, 1337);
ewBookFlight1337("Rando!");

// using event listeners
lufthansa.planes = 1000;  // the company has 1000 planes 
lufthansa.buyPlane = function () {
  console.log(this);
  this.planes++;  // whenever this is called, the number of planes is increased
  console.log(this.planes);
};

// HERE the this keyword points to the ELEMENT that called the handler
// in this case a BUTTON element; so it doesn't refer to the lufthansa obj
document
  .querySelector(".buy")
  .addEventListener('click',
    lufthansa.buyPlane.bind(lufthansa));  // the bind keyword solves the problem

// Partial application

// this returns a value with tax added
const addTax = (rate, value) => value + value * rate;

console.log(addTax(.05, 1000));

// the bind function could be used to PRESET the rate and create a partial app
// null is used since we're not interested in the this keyword
const addVAT = addTax.bind(null, 0.19);
console.log(addVAT(100)); // the tax rate is FIXED as it was bound with bind!

// Challenge - write a function that returns a function that does the above! TBC
const addTaxRate = function (rate) {
  return function (value) {
    return value + value * rate;
  }
}
console.log(addTaxRate(0.24)(100));


//= Immediately invoked Function Expressions =================================//
// functions that are called once immediately and then never again
(function () {
  console.log("This is now treated as an expression which is immeditely "
    + "called with the () at the end.")
})();

// using this with an arrow function
(() => console.log("This also will never run again!"))();

//= Closures =================================================================//
let secureBooking = function () {
  let passengerCount = 0;

  // the child function increments the variable from the parent function
  return function () {
    passengerCount++;
    console.log(`${passengerCount} passenger`);
  }
}

const bookingCounter = secureBooking();

// here, a function FROM the global context/scope is calling a function
// from a context that is CLOSED (as secureBooking() is closed after line 235) 
// the Closure aspect REMEMBERS all the variables that were available at the 
// creation of the function
bookingCounter();
bookingCounter();
bookingCounter();
console.dir(bookingCounter);

// another example of closure
let f;
const g = function () {
  const a = 23;
  f = function () {
    console.log(a * 2);
  }
}

const h = function () {
  const b = 666;
  f = function () {
    console.log(b * 2);
  }
}

g();  // this NEEDS to be called so that f() exists

// here f() returns 46 - f was defined outside, but as it is assigned 
// the value of a function inside the g() function, it can access the value of A
f();

h();
f();  // here, f() gets reassigned and 1332 is returned

// another closure example
const boardPassengers = function (numberOfPass, waitTime) {
  const perGroup = numberOfPass / 3;
  // using a timer - setTimeout executes a function it gets as a parameter
  // once the timer expires - time is expressed in ms
  setTimeout(function () {
    console.log(`We are now boarding all ${numberOfPass} passengers`);
    console.log(`There are 3 groups each with ${perGroup} passengers`);
  }, waitTime * 1000);  // this gets called AFTER 3 seconds
  console.log(`We will start boarding in ${waitTime} seconds`);
}

boardPassengers(180, 5);

