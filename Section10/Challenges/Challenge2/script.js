/**
 * Using the IIFE below, attach an event listener that changes the color of the
 * selected h1 element to blue each time the BODY element is clicked. Do not
 * select the h1 element again.
 *
 * Explain to yourself how this works.
 */


(function() {
  const header = document.querySelector("h1");
  header.style.color = "red";    

  header.parentElement.addEventListener("click", function() {
    header.style.color="blue";
  });  
})();

/**
 * Shit above is trippy. 
 * The below function immediately runs and sets the color of the h1 to red.
 * 
 * Despite the function finishing, there is still an event listener that is 
 * ATTACHED to the body element, so that when the body is clicked, the code
 * executes, although the function was already closed!
 * 
 * This code doesn't have to be tied to another function or variable since it 
 * is tied to the body!
 */