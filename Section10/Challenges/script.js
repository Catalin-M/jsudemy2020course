/**
 * Poll App:
 * 
 * A poll has a question, an array of options from which people can choose and
 * an array with the number of replies for each option. This data is stored in 
 * the starter object below.
 * 
 * 1. Crete a method called `registerNewAnswer` on the `poll` object. The method
 * does 2 things:
 *  1.1 display a prompt window for the user to input the number of the selected
 *  option. The prompt should look like this:
 *    0: JavaScript
 *    1: Python
 *    2: Rust
 *    3: C++
 *    (Write option number)
 *  1.2 Based on the input number, update the answers array. For ex if the
 *  answer is 3, increase the value at POSITION 3 of the array by 1. Make sure
 *  to check if the input is a number of if the number makes sense (52 wouldn't
 *  make sense)
 * 2. Call this method whenever the user clicks the "Answer poll" button
 * 3. Create a method "displayResults" which displays the poll results. The me-
 * thod takes a string as an input (called "type") which can be either "string"
 * or "array". If type is "array" simply display the results array as it is
 * using console.log(). This should be the default, if it's "string" 
 * then display a string like "Poll, results are 3, 22, 5"
 * 4. Run the "displayResults" at the end of each "registerNewAnswer" method
 * call
 * 
 * BONUS: Use the "displayResults" method to display the 2 arrays in the test 
 * data. Use both the "array" and the "string" option. Do NOT put the arrays in
 * the poll object. The way the this keyword look like:
 * 
 * Bonus test data 1: [5, 2, 3]
 * Bonus test data 2: [1, 5, 3, 9, 6, 1]
 */

const poll = {
  question: "What is your favorite programming language?",
  options: ["0: JavaScript", "1: Python", "2: Rust", "3: C++", "4: Java"],
  answers: new Array(5).fill(0),
  type: "string",

  // function will prompt user for their fav language and voting will
  // increase the count for that language; the results of the poll will be 
  // displayed in the log
  registerNewAnswer: function () {    
    let favoriteProgramming = Number(prompt("Please enter the number associated"
      + "with your favorite programming" 
      + " language: \n0: Javascript\n1: Python\n2: Rust\n3: C++\n4: Java"));

    // if the output is not a number or is too large of a number
    // then the answers aren't incremented!
    typeof favoriteProgramming === "number" && favoriteProgramming < 
      this.options.length && this.answers[favoriteProgramming]++;  
    this.displayResults(this.type);
  },

  // method receives the type of display; it should be array or string
  // it defaults to array
  displayResults(type = "array") {
    if (type == "array") {
      console.log(this.answers);      
    } else {
      console.log(`The poll results are ${this.answers.join(", ")}`);      
    }
  }
};

document.querySelector(".poll").addEventListener('click',
  poll.registerNewAnswer.bind(poll));

const testPoll = {
  answers: [5, 2, 3],
  type: "string"
}

// this manually sets the 'this' keyword to point to testPoll
poll.displayResults.call(testPoll, testPoll.type);