//
// for loops keeps running while the condition is true
for (let i=0; i<5; i++) {
    console.log(`Chest barbell exercise rep ${i}`);
}

const psyArray = [
    'pSYoniK',
    'Ghost',
    1999,
    'pro gamer',
    ['SK', 'mTw', '4K']
]

const copyArray = []

for (let i=0; i<psyArray.length; i++) {
    console.log(psyArray[i]);
    copyArray[i] = typeof psyArray[i];
}

console.log(copyArray);

const years = [1957, 1966, 1959, 1987, 1988];
const ages = [];

for (let i=0; i<years.length; i++) {
    ages.push(2020 - years[i]);
}
console.log(ages);

// continue and break offer additional control during a loop
// here, if the element in the array is not a string
// the code 'continues' to the next value instead printing only strings
for (let i=0; i<psyArray.length; i++) {
    if (typeof psyArray[i] !== 'string') continue;
    console.log(psyArray[i], typeof psyArray[i]);
}

// here if the element is not a string
// the code 'breaks' exiting the loop entirely!
console.log("The break loop: ");
for (let i=0; i<psyArray.length; i++) {
    if (typeof psyArray[i] !== 'string') break;
    console.log(psyArray[i], typeof psyArray[i]);
}