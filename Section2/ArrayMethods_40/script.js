//
// Adding elements
const friends = ['kk', 'cc'];
console.log(friends);
console.log('Initial length of array: ' 
    + friends.length);
console.log('Length after adding at the end: ' 
    + friends.push('qlr'));  //adds element at the end
console.log('Length after adding at the beginning: ' 
    + friends.unshift('jesus'));   //adds element at the beginning
console.log(friends);

//
// Removing elements
console.log('The element removed from the end: ' 
    + friends.pop());       //removes last element from the end
console.log('The elementremoved from the front: ' 
    + friends.shift());     //removes first element from the end
console.log(friends);

//
// Array index and accessing elements
console.log('The index at which kk is located: ' 
    + friends.indexOf('kk'));
console.log('The index at which a non-existent element is located at: ' 
    + friends.indexOf('nowhere'));
//
// More modern method that returns if an element is included in the array or not
// this returns true or false and uses STRICT equality 
console.log(friends.includes('kk'));