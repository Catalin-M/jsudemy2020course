// 'use strict';

// let hasDriversLicense = false;
// const passTest = true;

// if (passTest) 
//     hasDriversLicense = true;

// if (hasDriversLicense) console.log('I can drive');

// const interface = 'audio';
// const private = 1;

// 'use strict';
// function logger() {
//     console.log('My name is Catalin');
// }

// logger();

// function fruitProcessor(apples, oranges) {
//     console.log(apples, oranges);
//     const juice = `Juice with ${apples} apples and ${oranges} oranges`;
//     return juice;
// }

// console.log(fruitProcessor(3, 5));

// simple method that calculates an age given a birth year
// function calcAge1(birthYear) {
//     const age = 2020 - birthYear;
//     return age;
// }
// console.log(calcAge1(1987));

// // function expression
// const ageCalc = function (birthYear) {
//     return 2020 - birthYear;
// }
// console.log(ageCalc(1988));

// // a function can be called before it is actually written
// // a function expression needs to be reached before it can be called

// // ==> arrow function
// // this is very similar to function expressions
// // the value is automatically returned in an arrow function
// // this is excellent for one liner functions
// const arrowCalcAge = birthYear => 2020 - birthYear;

// console.log(arrowCalcAge(1959));

// const yearsToRetirement = retirementYear => {
//     const age = 2020 - retirementYear;
//     const retirement = 67 - age;
//     return retirement;
// }

// console.log(yearsToRetirement(1987));

// const monthsToBirthday = (birthMonth, name) => {
//     const monthsLeft = 12 - birthMonth;
//     return `${name}, you have ${monthsLeft} months until your birth day!`;
// }

// console.log(monthsToBirthday(8, "Catalin"));

// function cutFruitPieces(fruit) {
//     return fruit * 4;
// }

// function fruitProcessor(apples, oranges) {
//     const applePieces = cutFruitPieces(apples);
//     const orangePieces = cutFruitPieces(oranges);

//     const juice = `Juice with ${applePieces} apples and ${orangePieces} pieces of orange.`;
//     return juice;
// }

// console.log(fruitProcessor(2,3));


const ageCalculator = function(birthYear) {
    return 2020 - birthYear;
}

const retirementCalculator = function(age) {
    return 67 - age;
}

const yearsUntilRetirement = function(birthYear, firstName) {
    const age = ageCalculator(birthYear);
    const retirement = retirementCalculator(age);

    if (retirement>0) {
        return `${firstName} retires in ${retirement} years`;
    } 
    else {
        return `${firstName} has already retired`;
    }
}

console.log(yearsUntilRetirement(1947, "Catalin"));