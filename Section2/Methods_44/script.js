const psyonik = {
    firstName: 'pSYoniK',
    realName: 'Catalin',
    mmr: 4500,
    friends: ['Cici', 'Ciof', 'Cicic'],
    runsLinux: "Linux",

    calcMmr: function(mmr) {
        return mmr+500;
    },

    calcThisMmr: function() {
        return this.mmr + 1000;
    },

    calcNewMmr: function() {
        this.newMmr = this.mmr + 1500;
        return this.newMmr;
    },

    getSummary: function() {
        return `${this.firstName} is actually called ${this.realName}, ` + 
            `has an mmr of ${this.mmr}, ` + 
            `uses ${this.runsLinux} as an OS ` +
            `and has the following friends ${this.friends}`;
    }
}

console.log(psyonik.calcMmr(2000));
console.log(psyonik['calcMmr'](2000));
console.log(psyonik.calcThisMmr());
console.log(psyonik['calcThisMmr']());
psyonik.calcNewMmr();
console.log(psyonik.newMmr);
console.log(psyonik);
console.log(psyonik.getSummary());