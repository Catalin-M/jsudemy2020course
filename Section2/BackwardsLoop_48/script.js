const countries=[
    "Portugal",
    "Spain",
    "Palma",
    "Italy",
    "Croatia",
    "Madeira"
]

// this time I will just iterate through the list from back to front
for (let i=countries.length-1; i>=0; i--) {
    console.log(countries[i]);
}

// the while loop
let reps = 0;
while (reps<=10) {
    console.log(`This is rep number ${reps}. ${10-reps} left!`);
    reps++;
}