/**
 * Steven is building a tip calculator with the following rules:
 * Tip 15% of the bill if the bill value is between 50 and 300
 * Tip 20% for any other values
 * 
 * 1. write a function 'calcTip' that takes any bill value as input
 * and outputs the corresponding tip 
 * 2. Create an array bills containing 125, 555 and 44
 * 3. create array tips contianing the tip value for each bill
 * 4. create an array total containing the total values bill+tip
 */

 function calcTip(bill) {
     if (bill >= 50 && bill <= 300) {
         return bill * 0.15;
     }
     else {
         return bill * 0.2;
     }
 }
 
 const bills = [125, 555, 44];
 const tips = [calcTip(bills[0]), calcTip(bills[1]), calcTip(bills[2])];
 const total = [bills[0]+tips[0], bills[1]+tips[1], bills[2]+tips[2]];

 console.log(bills);
 console.log(tips);
 console.log(total);

