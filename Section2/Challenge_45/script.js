/**
 * BMI = mass / height ** 2  = mass / (height * height)
 * mass is expressed in kg and height in meters
 * 
 * 1. For each person create an object with properties that hold 
 * their name, weight and height
 * 2. Create a 'calcBMI' method for each of the members that return 
 * the value and also assign it to a property on each object
 * 3. Log the name of the person with the higher BMI to the console
 * TEST DATA: Mark weighs 78kgs and is 1.69m tall
 * John weighs 92kgs and is 1.95m tall
 */

 const mark = {
    name: "Mark",
    weight: 78,
    height: 1.69,

    calcBMI: function() {
        this.bmi = this.weight/(this.height * this.height);
        return this.bmi;
    }
 }
 mark.calcBMI();

 const john = {
     name: "John",
     weight: 92,
     height: 1.95,

     calcBMI: function() {
         this.bmi = this.weight/(this.height * this.height);
         return this.bmi;
     }
 }
 john.calcBMI();

 if (mark.bmi > john.bmi) {
    console.log(`Mark's BMI of ${mark.bmi} is higher ` +
        `than John's BMI of ${john.bmi}`);
 }
 else {
     console.log(`John's BMI of ${john.bmi} is higher ` +
        `than Mark's BMI of ${mark.bmi}`);
 }