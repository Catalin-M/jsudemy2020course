// objects can initially be thought of as collections
// of key value pairs in JS

const catalin = {
    firstName: 'Catalin',
    nickName: 'pSYoniK',
    age: 33,
    job: 'developer',
    favoriteGames: ['Path of Exile', 'Dota 2', 'Quake Champions']
}

const friends = ['Cici', 'Ciof', 'Cicic'];
catalin.friends = friends;

catalin.location = 'Scotland';
catalin['website'] = 'catalinm.herokuapp.com';
console.log(catalin);

// challenge
// write Catalin has 3 friends and his best friend is called
// cici
// i first created an array of 3 friends
// and then created a property called friends inside the object and assigned 
// the array to that property
console.log(catalin.firstName + ' has ' + catalin.friends.length + ' friends and '
    + 'his best friend is ' + catalin.friends[0]);
console.log(`${catalin.firstName} has ${catalin.friends.length} ` +
    `friends and his best friend is ${catalin.friends[0]}`);
