//
// arrays are one of the most important data structures in Javascript

const friends = ['kk', 'cc', 'puy'];
const years = [1987, 1988, 1994, 2020];
const newYears = new Array(1066, 1805, 1928);

console.log(friends);
console.log(years);
console.log(newYears);

friends[2] = 'Puyulete';

console.log(friends);

const catalin = ['Catalin', 'Edinburgh', 2020-1918];
console.log(catalin);

// Exercise
const calculateAge = function (yearOfBirth) {
    return 2020-yearOfBirth;
}


// the function can be used to store the years values inside a new array
// -> This would not work => calculateAge(years);
// if the values need to be saved inside a new array, we call the function on all
// the elements inside the array
const ages = [calculateAge(years[0]), calculateAge(years[1]), calculateAge(years[2])];
console.log(ages);