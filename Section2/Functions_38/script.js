/**
 * Coding challenge
 * 
 * There are 2 gymnastics teams
 * Each team competes 3 times and then the average of the 3 scores is calculated
 * so you have one average per team.
 * 
 * A team ONLY wins if it has at least DOUBLE the average score of the other team.
 * Otherwise, no team wins!
 * 
 * 1. Create arrow function 'calcAverage' to calculate average of the 3 scores
 * 2. Use the function to calculate the average for both teams
 * 3. Create a function 'checkWinner' that takes the average score of each team
 * as parameters ('avgDolphins' and 'avgKoalas'), and then logs the winner
 * to the console together with the victory points according to the above rule.
 * EX: Koalas win (30 vs 13)
 * 4. Use the 'checkWinner' function to determine the winner for both DATA 1 and
 * DATA 2
 * 5. Ignore Draws
 * 
 * DATA 1: Dolphins score 44, 23, 71; Koalas score 65, 54, 49
 * DATA 2: Dolphins score 85, 54, 41; Koalas score 23, 34, 27
 * 
 */

 const dolphinsScores1 = [44, 23, 71];
 const koalasScores1 = [65, 54, 49];

 const dolphinsScores2 = [85, 54, 41];
 const koalasScores2 = [23, 34, 27];

 const calcAverage = (score1, score2, score3) => {
     return (score1 + score2 + score3) / 3;
 }

 function checkWinner(avgDolphins, avgKoalas) {
    if (avgDolphins > avgKoalas * 2) {
        console.log(`The Dolphins have won with ${avgDolphins} points`);
    }
    else if (avgKoalas > avgDolphins * 2) {
        console.log(`The Koalas have won with ${avgKoalas} points`);
    }
 }
 const avgDolphins = calcAverage(dolphinsScores2[0], dolphinsScores2[1], dolphinsScores2[2]);
 const avgKoalas = calcAverage(koalasScores2[0], koalasScores2[1], koalasScores2[2]);
 console.log(avgDolphins);
 console.log(avgKoalas);
 checkWinner(avgDolphins, avgKoalas);