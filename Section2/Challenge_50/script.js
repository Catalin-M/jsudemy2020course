/**
 * Tip 15% of the bill if the bill value is between 50 and 300
 * Tip 20% for any other values
 * 1. Create an array 'bills' for all 10 bill values
 * 2. Create empty arrays for the 10 tips and totals
 * 3. Create a calcTips function to calculate tips and totals
 * for every bill in the 'bills' array
 * bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52]
 * 4. Write function calcAverage that takes an array arr as input
 * which then calculates the average for all the elements in the arr
 */

function calcTips(billAmount) {
  if (billAmount < 50 || billAmount > 300) {
    return billAmount * 0.15;
  } else {
    return billAmount * 0.2;
  }
}

function calcAverage(inputArray) {
  let totalAmount = 0;
  for (let x of inputArray) {
    totalAmount += x;
  }
  return totalAmount / inputArray.length;
}

const bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52];
const tips = [];
const totals = [];

for (let i = 0; i < bills.length; i++) {
  tips.push(calcTips(bills[i]));
  totals.push(tips[i] + bills[i]);
}

const average = calcAverage(totals);

console.log("Average of bills is: " + average);
console.log("The list of tips is: " + tips);
console.log("The list of totals is: " + totals);
